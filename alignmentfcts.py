from beamlinetools.beamline_config import *
from bluesky.plan_stubs import mv, wait
from bluesky.plans import (
    count,
    scan, scan as ascan,
    relative_scan as dscan,
    rel_grid_scan as dmesh,
    grid_scan as amesh,
    inner_product_scan as a2scan,
    relative_inner_product_scan as d2scan,
    tweak)

import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator, FixedLocator, FormatStrFormatter

import numpy as np
import scipy.interpolate as inter

from lmfit.models import LinearModel, GaussianModel, VoigtModel, SkewedVoigtModel, Model
from lmfit import Parameters

from beamlinetools.utils.undulator_fit import UndulatorFit

from epics import caget, caput, cainfo

def getKeithleycurrents(avgs=5):

    yield from count([kth1,kth2,kth3], num = avgs) 
    run = db[-1]
    data = run.primary.read()

    avread = [float(np.mean(data["kth1"].data)), float(np.mean(data["kth2"].data)), float(np.mean(data["kth3"].data))]
    return avread



def SMU_Rx_Scan(rx_start = 29.5,rx_end = 31.5,rx_lower_limit = 28,rx_upper_limit = 33,steps_coarse = 20,steps_fine = 30):

    def smu_rx_move(target):
        if rx_lower_limit <= target <= rx_upper_limit:
            yield from mv(m1.rx,target,group='SMUmove')
            yield from wait('SMUmove')
        else: 
            print("target position out of range!")
            
    oldM1Rx = float(m1.rx.readback.value)
    #print("old M1(SMU)-Rx was",oldM1Rx, "at a mirror current of", getKeithleycurrents()[0] )

    try:
        if not rx_lower_limit <= rx_start <= rx_upper_limit:
            rx_start = 30
        if not rx_lower_limit <= rx_end <= rx_upper_limit:
            rx_end = 31.5

        yield from scan([kth1], m1.rx, rx_start, rx_end, steps_coarse)
        
        ################ UndulatorFit.retrieve_spectra #############
        run = db[-1]
        data = run.primary.read()
        x,y = data['m1_rx'], data['kth1']
        
        mod = VoigtModel()
        guess = {"amp1":np.max(y)}
        pars = mod.make_params( amplitude=guess['amp1'],
                                center=31,sigma=0.05,gamma=0.05)
        out = mod.fit(y,pars,x=x)
        delta = out.eval_uncertainty(x=x)

        plt.gcf()
        #plt.rc("font", size=12,family='serif')
        #fig, axes = plt.subplots(2, 1, figsize=(8.0, 16.0))
        #plt.suptitle('Scan n:'+self.spec_number+' uid:'+self.bluesky_id)
        
        plt.scatter(x, y, label='data', s=10)
        plt.plot(x, out.best_fit, 'b', label='best fit')
        #plt.fill_between(x,out.best_fit-delta,out.best_fit+delta,color='gray',alpha=0.4, label='fit uncertainty')

        plt.legend()
        #plt.xlabel('SMU Rx')
        #plt.ylabel('Intensity [a.u.]')

        plt.ylim(0.9*np.min(y),1.1*np.max(y))
        plt.tick_params(axis='both',direction='in',length=6,top=True,right=True)

        x0 = out.best_values['center']
        if rx_lower_limit <= x0 <= rx_upper_limit:
            fwhm = np.round(2*out.best_values['sigma'],2)   # FIX THIS!!
            fine_start = np.round(x0-fwhm,2)
            fine_end = np.round(x0+fwhm,2)
            fine_steps = int((fine_end-fine_start)/0.01)
            print(x0-6*fwhm)
            yield from smu_rx_move(x0-6*fwhm)
            print(fine_start)
            yield from smu_rx_move(fine_start)
            yield from scan([kth1], m1.rx, fine_start, fine_end, steps_fine)
        else:
            print("fit position out of range! Try find manually")

        ################ UndulatorFit.retrieve_spectra #############
        run = db[-1]
        data = run.primary.read()
        x_fine,y_fine = data['m1_rx'], data['kth1']
        
        guess = {"amp1":np.max(y_fine)}
        pars = mod.make_params( amplitude=guess['amp1'],
                                center=31,sigma=out.best_values['sigma'],gamma=out.best_values['gamma'])
        out2 = mod.fit(y_fine,pars,x=x_fine)
        delta2 = out2.eval_uncertainty(x=x_fine)
        x0 = out2.best_values['center']

        plt.scatter(x_fine, y_fine, label='data_fine', s=10)
        plt.plot(x_fine, out2.best_fit, 'r', label='best fit')

        if rx_lower_limit <= x0 <= rx_upper_limit:
            newM1Rx = x0
            print("new Rx position",newM1Rx)
            #mc_new = getKeithleycurrents()[0]
            #print("new M1(SMU)-Rx was",newM1Rx, "at a mirror current of", mc_new)
            yield from smu_rx_move(newM1Rx)                                            #TODO:  Why this is not executed?!?!?!
        else:
            print("fit position out of range! Try find manually")
    except KeyboardInterrupt:
        print('You stopped me!')
    except Exception as e:
        print('Error: SMU_Rx_Scan: ',e)
        yield from smu_rx_move(oldM1Rx)
        return()


def setASBL(target, offset_ver=0, offset_hor=0):
    yield from mv(asbl.top,target+offset_ver,
                asbl.bottom,-target+offset_ver,
                asbl.left,target+offset_hor,
                asbl.right,-target+offset_hor)


def asbl_Scan():
    yield from setASBL(2)
    yield from scan([kth1], asbl.top, 2, -2, 41)
    yield from setASBL(2)
    yield from scan([kth1], asbl.bottom, -2, 2, 41)
    yield from setASBL(2)
    yield from scan([kth1], asbl.left, 2, -2, 41)
    yield from setASBL(2)
    yield from scan([kth1], asbl.right, -2, 2, 41)
    yield from setASBL(2)


def Escan(E_start ,E_stop ,Espeed ,E_step ,contmode=True):
    
    E = pgm.en.get()
    if contmode = False:
        yield from scan([kth1, kth2, kth3], ??mono??, E_start = ,E_stop = , E_step =) 

    if contmode = True:
        yield from flyscan([kth1, kth2, kth3], pgm.en, E_start = ,E_stop = ) 
    mv(pgm.en,E)

def UndulatorScan(E):

    yield from setASBL(2)

    print("slope: ", )      # TO BE DEFINED!! 
    print("offset: ", )      # TO BE DEFINED!! 
    print("ID gap: ", )      # TO BE DEFINED!! 

    yield from IDoff        # TO BE DEFINED!!
    yield from flyscan([kth1], pgm.en, E_start = E-8,E_stop = E+8)

    yield from mv(pgm.en,E)
    energy, mod, out, delta = fit_undulator(-1)
    
    ############ find peak position on finer energy grid ################
    xfit = np.arange(min(energy),max(energy), 0.01)
    yfit = ufit.skewed_voigt(x=xfit, amplitude = out.best_values['a1'], center = out.best_values['c1'], sigma = out.best_values['sigma'], gamma = out.best_values['gamma'], skew =  out.best_values['skew'])
    peakpos = xfit[np.argmax(yfit)]
    print("Undulator scan around {} finds peak at {}".format(E,peakpos))

    return peakpos

def SlopeOffsetCorr(E_cen):

    E_min = E_cen-10
    E_max = E_cen+10

    peakpos1 = UndulatorScan(E_min)
    peakpos2 = UndulatorScan(E_max)



#def shutdown_beamline():

def ue112_commissioning(E_min=, E_max= ):

    yield from SMU_Rx_Scan()

    yield from asbl_Scan()
    offset_vertical = intersection(-4,-3)
    offset_horizontal = intersection(-2,-1)
    print("new offsets are \n offset_horizontal: {}\n offset_vertical {}", .format(offset_horizontal, offset_vertical))

    peakpos1 = UndulatorScan(E_min)
    caput('ue112pgm1:Energy1',E_min)
    caput('ue112pgm1:Meas1',peakpos1)
    peakpos2 = UndulatorScan(E_max)
    caput('ue112pgm1:Energy2',E_max)
    caput('ue112pgm1:Meas2',peakpos2)

    caget('ue112pgm1:CalcSlope.PROC',1)
    print("old slope/offset parameters:")
    caget('ue112pgm1:OldSlope')
    caget('ue112pgm1:OldOffset')

    print("new calculated slope/offset parameters:")
    caget('ue112pgm1:NewSlope')
    caget('ue112pgm1:NewOffset')

    print('Do you accept the new parameters? Y/N')
    check = input()
    if check == 'Y':
        print("new values accepted!")
        caput('ue112pgm1:AcceptSO.PROC', 1)
    else:
        print("old values restored!")
        caput('ue112pgm1:UseCurSO.PROC', 1)

    
