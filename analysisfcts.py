from beamlinetools.beamline_config import *

import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator, FixedLocator, FormatStrFormatter

import numpy as np
import scipy.interpolate as inter

from lmfit.models import LinearModel, GaussianModel, VoigtModel, SkewedVoigtModel, Model
from lmfit import Parameters

from beamlinetools.utils.undulator_fit import UndulatorFit



def intercept(id1=-1,id2=-2, x_min=-2, x_max=2, x_step=0.001):
                                                    # TODO export getSpectra function
    
    run = db[id1]
    data1 = run.primary.read()
    motor = run.metadata['start']['motors'][0]
    detector  = run.metadata['start']['detectors'][0]
    x1 = np.array(data1[motor])
    y1 = np.array(data1[detector])

    run = db[id2]
    data2 = run.primary.read()
    motor = run.metadata['start']['motors'][0]
    detector  = run.metadata['start']['detectors'][0]
    x2 = np.array(data2[motor])
    y2 = np.array(data2[detector])

    cs1 = inter.CubicSpline(-1*x1,y1)
    cs2 = inter.CubicSpline(x2,y2)               # TODO: aks if xs is monotonically increasing befire cs

    xs = np.arange(x_min, x_max, x_step)
    sect = xs[np.argmin(abs(cs1(-1*xs)-cs2(xs)))]
    plt.rc("font", size=12,family='serif')
    fig = plt.figure()
    plt.scatter(x1,y1, color='tab:blue', marker='x', s=20, label='scan')
    plt.scatter(x2,y2, color='tab:orange', marker='x', s=20)
    plt.plot(xs, cs1(-1*xs), color='tab:blue', label='interpolation')
    plt.plot(xs, cs2(xs), color='tab:orange' )
    plt.plot(xs, abs(cs1(-1*xs)-cs2(xs)), color='tab:green')
    plt.axvline(sect, color='tab:green', linestyle=':')
    plt.legend()

    print("intersection at {:.3f} with precision of {}".format(sect, x_step))
    return sect

#def loopZscan(x_min, x_max, )

def fit_derivative(scannr, guess_cen,data_min, data_max, yflip=True):
    run = db[scannr]
    data = run.primary.read()
    x,y = data['expchamber_z'], data['kth2']
    y_der = np.gradient(y,x)
    if yflip == True:
        flip = -1
    else:
        flip = 1

    mod = GaussianModel()
    guess = {"amp1":np.max(flip*y_der), "c1":x[np.argmax(flip*y_der)]}
    pars = mod.make_params( amplitude=dict(value=guess['amp1'], min=0),
                            center=guess['c1'],#, min=x[data_min], max=x[data_max] TODO: organize that x[data_min] always smaller than x[data_max] !!
                            sigma=0.002)
    out = mod.fit(flip*y_der[data_min:data_max],pars,x=x[data_min:data_max])
    delta = out.eval_uncertainty(x=x)
    
    print(out.fit_report(min_correl=0.5))

    plt.rc("font", size=12,family='serif')
    fig, ax1 = plt.subplots()
    
    colors = ['tab:blue', 'tab:orange']
    ax1.set_xlabel('expchamber_z [mm]')
    ax1.set_ylabel('kth2 [A]', color=colors[0])
    ax1.scatter(x, y, label='data', s=10)

    ax2 = ax1.twinx()
    ax2.set_ylabel('derivative', color=colors[1])
    ax2.scatter(x, flip*y_der, marker='x', color=colors[1], label='derivative', s=20)
    #plt.plot(x[data_min:data_max], out.init_fit, '--', label='initial fit')
    ax2.plot(x[data_min:data_max], out.best_fit, 'r', label='best fit')
    fig.tight_layout()
    fig.legend()
    return out